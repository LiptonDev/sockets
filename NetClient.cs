﻿using Sockets.Delegates;
using System;
using System.Net.Sockets;

namespace Sockets
{
    /// <summary>
    /// The async TCP sockets client.
    /// </summary>
    public class NetClient
    {
        /// <summary>
        /// Enable validation of inbound data. 
        /// </summary>
        public bool DataValidation
        {
            set
            {
                if (IsConnected)
                    throw new Exception("cannot change data validation when a client is connected");

                NetStream.UseDataValidation = value;
            }
            get
            {
                return NetStream.UseDataValidation;
            }
        }

        /// <summary>
        /// TCP Client.
        /// </summary>
        TcpClient tcp;

        /// <summary>
        /// Work stream.
        /// </summary>
        NetStream stream;

        /// <summary>
        /// Remote host (IP).
        /// </summary>
        public string RemoteHost { get; private set; }

        /// <summary>
        /// Remote port.
        /// </summary>
        public int RemotePort { get; private set; }

        /// <summary>
        /// Gets a value that indicates whether the client is connected to the server.
        /// </summary>
        public bool IsConnected { get; private set; }

        /// <summary>
        /// Occurs when connecting to the server.
        /// </summary>
        public event ClientOnConnected OnConnected = delegate { };

        /// <summary>
        /// Occurs when disconnecting to the server.
        /// </summary>
        public event ClientOnDisconnected OnDisconnected = delegate { };

        /// <summary>
        /// Occurs when data is received from the server.
        /// </summary>
        public event ClientOnReceived OnReceived = delegate { };

        /// <summary>
        /// Initializes a new instance of the NetClient.
        /// </summary>
        public NetClient()
        {
            IsConnected = false;
        }

        /// <summary>
        /// Connect to the server.
        /// </summary>
        /// <param name="Host">IP</param>
        /// <param name="Port">Port</param>
        public void Connect(string Host, int Port)
        {
            if (IsConnected)
            {
                Disconnect();
            }

            RemoteHost = Host;
            RemotePort = Port;
            tcp = new TcpClient()
            {
                SendBufferSize = NetStream.MaxDataLength,
                ReceiveBufferSize = NetStream.MaxDataLength,
                ReceiveTimeout = 0,
                SendTimeout = 0
            };

            tcp.Connect(Host, Port);
            IsConnected = true;

            stream = new NetStream(tcp.GetStream(), tcp.Client.RemoteEndPoint);

            stream.OnReceived += stream_OnReceived;
            stream.OnStarted += stream_OnStarted;
            stream.OnStopped += stream_OnStopped;

            stream.Start();
        }

        /// <summary>
        /// Client disconnected.
        /// </summary>
        /// <param name="Sender">client stream</param>
        /// <param name="Client">client GUID</param>
        void stream_OnStopped(NetStream Sender, Guid Client)
        {
            OnDisconnected(this);
        }

        /// <summary>
        /// Client connected.
        /// </summary>
        /// <param name="Sender">client stream</param>
        /// <param name="Client">clint GUID</param>
        void stream_OnStarted(NetStream Sender, Guid Client)
        {
            OnConnected(this);
        }

        /// <summary>
        /// Data received.
        /// </summary>
        /// <param name="Sender">client stream</param>
        /// <param name="Client">clint GUID</param>
        /// <param name="Data">data</param>
        void stream_OnReceived(NetStream Sender, Guid Client, byte[] Data)
        {
            OnReceived(this, Data);
        }

        /// <summary>
        /// Disconnect from the server.
        /// </summary>
        public void Disconnect()
        {
            if (!IsConnected) return;

            IsConnected = false;

            stream.Stop();
            tcp.Close();
        }

        /// <summary>
        /// Send data to the server.
        /// </summary>
        /// <param name="Data">data</param>
        /// <returns></returns>
        public bool Send(byte[] Data)
        {
            if (IsConnected)
                stream.Send(Data);

            return IsConnected;
        }

        /// <summary>
        /// Releasing resources.
        /// </summary>
        public void Dispose()
        {
            Disconnect();

            tcp = null;
            stream = null;
        }
    }
}
