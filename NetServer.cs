﻿using Sockets.Delegates;
using Sockets.Enums;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Sockets
{
    /// <summary>
    /// The async TCP sockets server.
    /// </summary>
    public class NetServer
    {
        /// <summary>
        /// Initializes a new instance of the NetServer with echo mode.
        /// </summary>
        /// <param name="mode">echo mode</param>
        public NetServer(EchoMode mode)
        {
            EchoMode = mode;
        }

        /// <summary>
        /// Initializes a new instance of the NetServer.
        /// </summary>
        public NetServer()
        {

        }

        /// <summary>
        /// Server echo mode.
        /// </summary>
        public EchoMode EchoMode { get; private set; }

        /// <summary>
        /// Enable validation of inbound data. 
        /// </summary>
        public bool DataValidation
        {
            set
            {
                if (active)
                    throw new Exception("cannot change data validation when the server is running");

                NetStream.UseDataValidation = value;
            }
            get
            {
                return NetStream.UseDataValidation;
            }
        }

        /// <summary>
        /// Clients GUIDs.
        /// </summary>
        List<Guid> clients = new List<Guid>();

        /// <summary>
        /// Clients streams.
        /// </summary>
        ConcurrentDictionary<Guid, NetStream> streams = new ConcurrentDictionary<Guid, NetStream>();

        /// <summary>
        /// TCP server.
        /// </summary>
        TcpListener tcp;

        /// <summary>
        /// Server status (enabled/disabled).
        /// </summary>
        bool active = false;

        /// <summary>
        /// Gets the listening port number.
        /// </summary>
        public int Port { get; private set; }

        /// <summary>
        /// Gets a value that indicates whether the server is online.
        /// </summary>
        public bool IsOnline
        {
            get
            {
                return active;
            }
        }

        /// <summary>
        /// Tick rate.
        /// </summary>
        int tickRate = 1;

        /// <summary>
        /// Gets the address of the listening.
        /// </summary>
        public IPAddress Address { get; private set; }

        /// <summary>
        /// Gets an array of GUIDs of connected clients.
        /// </summary>
        public Guid[] ClientArray
        {
            get
            {
                return clients.ToArray();
            }
        }

        /// <summary>
        /// Gets an array of GUIDs of connected clients.
        /// </summary>
        public List<Guid> ClientList
        {
            get
            {
                return clients;
            }
        }

        /// <summary>
        /// Gets the number of connected clients.
        /// </summary>
        public int ClientCount
        {
            get
            {
                return clients.Count;
            }
        }

        /// <summary>
        /// Gets a dictionary of client streams.
        /// </summary>
        public ConcurrentDictionary<Guid, NetStream> ClientStreams
        {
            get
            {
                return streams;
            }
        }

        /// <summary>
        /// Occurs when the server starts.
        /// </summary>
        public event ServerOnStarted OnStarted = delegate { };

        /// <summary>
        /// Occurs when the server stop.
        /// </summary>
        public event ServerOnStopped OnStopped = delegate { };

        /// <summary>
        /// Occurs when data is received from the client.
        /// </summary>
        public event ServerOnReceived OnReceived = delegate { };

        /// <summary>
        /// Occurs when a new client is connected.
        /// </summary>
        public event ServerOnClientConnected OnClientConnected = delegate { };

        /// <summary>
        /// Occurs when a client is disconnected.
        /// </summary>
        public event ServerOnClientDisconnected OnClientDisconnected = delegate { };

        /// <summary>
        /// Starts the server at the specified port.
        /// </summary>
        /// <param name="port">port</param>
        public void Start(int port)
        {
            Start(IPAddress.Any, port);
        }

        /// <summary>
        /// Starts the server at the address and port.
        /// </summary>
        /// <param name="address">address</param>
        /// <param name="port">port</param>
        public void Start(IPAddress address, int port)
        {
            if (active)
                throw new Exception("Server already started");

            Address = address;
            Port = port;
            active = true;
            tcp = new TcpListener(address, port);
            tcp.Start();

            OnStarted(this);

            AcceptClient();
        }

        /// <summary>
        /// Stop the server.
        /// </summary>
        public void Stop()
        {
            if (!IsOnline) return;

            DisconnectAll();

            if (tcp != null) 
                tcp.Stop();

            active = false;

            OnStopped(this);
        }

        /// <summary>
        /// Waiting for new clients to connect.
        /// </summary>
        async void AcceptClient()
        {
            while (active)
            {
                await Task.Delay(tickRate);

                TcpClient client = null;
                try
                {
                    client = await tcp.AcceptTcpClientAsync();
                    client.SendBufferSize = NetStream.MaxDataLength;
                    client.ReceiveBufferSize = NetStream.MaxDataLength;
                    client.ReceiveTimeout = 0;
                    client.SendTimeout = 0;
                }
                catch (SocketException)
                {
                    if (client != null)
                        client.Close();
                    continue;
                }

                NetStream stream = new NetStream(client.GetStream(), client.Client.RemoteEndPoint);

                stream.OnStopped += stream_OnStopped;
                stream.OnStarted += stream_OnStarted;
                stream.OnReceived += stream_OnReceived;

                stream.Start();
            }
        }

        /// <summary>
        /// Client connected.
        /// </summary>
        /// <param name="Sender">client stream</param>
        /// <param name="Client">client GUID</param>
        void stream_OnStarted(NetStream Sender, Guid Client)
        {
            clients.Add(Client);
            streams.TryAdd(Client, Sender);

            OnClientConnected(this, Client);
        }

        /// <summary>
        /// Client disconnected.
        /// </summary>
        /// <param name="Sender">client stream</param>
        /// <param name="Client">client GUID</param>
        void stream_OnStopped(NetStream Sender, Guid Client)
        {
            OnClientDisconnected(this, Client);

            clients.Remove(Client);

            NetStream del;
            streams.TryRemove(Client, out del);
            del = null;
            Sender = null;
        }

        /// <summary>
        /// Received data from the client.
        /// </summary>
        /// <param name="Sender">client stream</param>
        /// <param name="Client">client GUID</param>
        /// <param name="Data">data</param>
        void stream_OnReceived(NetStream Sender, Guid Client, byte[] Data)
        {
            OnReceived(this, Client, Data);

            if (EchoMode == EchoMode.None) 
                return;

            switch (EchoMode)
            {
                case EchoMode.EchoAll:
                    DispatchAll(Data).Wait();
                    break;

                case EchoMode.EchoAllExceptSender:
                    DispatchAllExcept(Client, Data).Wait();
                    break;

                case EchoMode.EchoSender:
                    DispatchTo(Client, Data).Wait();
                    break;

                case EchoMode.AsyncEchoAll:
                    DispatchAll(Data);
                    break;

                case EchoMode.AsyncEchoAllExceptSender:
                    DispatchAllExcept(Client, Data);
                    break;

                case EchoMode.AsyncEchoSender:
                    DispatchTo(Client, Data);
                    break;
            }
        }

        /// <summary>
        /// Send data to a client.
        /// </summary>
        /// <param name="Client">client GUID</param>
        /// <param name="Data">data</param>
        public async Task DispatchTo(Guid Client, byte[] Data)
        {
            await Task.Delay(tickRate);

            if (streams.ContainsKey(Client))
                streams[Client].Send(Data);
        }

        /// <summary>
        /// Send data for some clients.
        /// </summary>
        /// <param name="Clients">clients GUIDs</param>
        /// <param name="Data">data</param>
        public async Task DispatchTo(Guid[] Clients, byte[] Data)
        {
            await Task.Delay(tickRate);

            Parallel.For(0, Clients.Length, async (x) =>
            {
                await DispatchTo(Clients[x], Data);
            });
        }

        /// <summary>
        /// Send data for some clients.
        /// </summary>
        /// <param name="Clients">clients GUIDs</param>
        /// <param name="Data">data</param>
        public async Task DispatchTo(List<Guid> Clients, byte[] Data)
        {
            await Task.Delay(tickRate);

            Parallel.For(0, Clients.Count, async (x) =>
            {
                await DispatchTo(Clients[x], Data);
            });
        }

        /// <summary>
        /// Send data to all clients.
        /// </summary>
        /// <param name="Data">data</param>
        public async Task DispatchAll(byte[] Data)
        {
            await DispatchTo(clients, Data);
        }

        /// <summary>
        /// Send data to all clients except for one client.
        /// </summary>
        /// <param name="Client">the GUID of the client to whom the data will not be sent</param>
        /// <param name="Data">data</param>
        public async Task DispatchAllExcept(Guid Client, byte[] Data)
        {
            await DispatchTo(clients.ListWhere(x => x != Client), Data);
        }

        /// <summary>
        /// Disconnect the client.
        /// </summary>
        /// <param name="Client">the GUID of the client who will be disabled</param>
        public void DisconnectClient(Guid Client)
        {
            if (streams.ContainsKey(Client))
                streams[Client].Stop();
        }

        /// <summary>
        /// Disconnect all clients except for one client.
        /// </summary>
        /// <param name="Client">the GUID of the client who will not be disabled</param>
        public void DisconnectAllExcept(Guid Client)
        {
            Parallel.For(0, clients.Count, x =>
            {
                if (clients[x] != Client)
                    DisconnectClient(clients[x]);
            });
        }

        /// <summary>
        /// Disconnect all clients.
        /// </summary>
        public void DisconnectAll()
        {
            while (clients.Count > 0)
                streams[clients[0]].Stop();
        }

        /// <summary>
        /// Releasing resources.
        /// </summary>
        public void Dispose()
        {
            Stop();

            tcp = null;
            clients.Clear();
            clients = null;
            streams.Clear();
            streams = null;
            Address = null;
        }
    }
}
