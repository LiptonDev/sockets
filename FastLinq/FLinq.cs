﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Sockets
{
    static class FLinq
    {
        /// <summary>
        /// Change the order of items on the reverse.
        /// </summary>
        /// <typeparam name="T">type</typeparam>
        /// <param name="arr">array</param>
        /// <returns></returns>
        public static T[] Reverse<T>(this T[] arr)
        {
            int len = arr.Length;

            T[] revArr = new T[len];

            Parallel.For(0, len, x =>
            {
                revArr[len - 1 - x] = arr[x];
            });

            return revArr;
        }

        /// <summary>
        /// Determines whether two sequences are equal by comparing their elements by using the default equality comparer that is intended for their type.
        /// </summary>
        /// <typeparam name="T">type</typeparam>
        /// <param name="first">first array</param>
        /// <param name="second">second array</param>
        /// <returns></returns>
        public static bool SequenceEqual<T>(this T[] first, T[] second)
        {
            if (first.Length < second.Length || first.Length > second.Length)
                return false;

            bool equal = true;

            Parallel.For(0, first.Length, x =>
            {
                if (!first[x].Equals(second[x]))
                {
                    equal = false;
                    return;
                }
            });

            return equal;
        }

        /// <summary>
        /// Filters a sequence of values based on the specified predicate.
        /// </summary>
        /// <typeparam name="T">type</typeparam>
        /// <param name="arr">list to filter</param>
        /// <param name="predicate">a function to test each element for a condition</param>
        /// <returns></returns>
        public static List<T> ListWhere<T>(this List<T> arr, Predicate<T> predicate)
        {
            List<T> objects = new List<T>();

            Parallel.For(0, arr.Count, x =>
            {
                if (predicate(arr[x]))
                    objects.Add(arr[x]);
            });

            return objects;
        }
    }
}
