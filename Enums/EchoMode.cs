﻿namespace Sockets.Enums
{
    /// <summary>
    /// Echo modes.
    /// </summary>
    public enum EchoMode
    {
        /// <summary>
        /// Without mode.
        /// </summary>
        None,

        /// <summary>
        /// Send to all clients.
        /// </summary>
        EchoAll,

        /// <summary>
        /// Send to all clients except the sender.
        /// </summary>
        EchoAllExceptSender,

        /// <summary>
        /// Send only to the sender.
        /// </summary>
        EchoSender,

        /// <summary>
        /// Send to all clients and continue processing without waiting for the send.
        /// </summary>
        AsyncEchoAll,

        /// <summary>
        /// Send to all clients except the sender and continue processing without waiting for the send.
        /// </summary>
        AsyncEchoAllExceptSender,

        /// <summary>
        /// Send only to the sender and continue processing without waiting for the send.
        /// </summary>
        AsyncEchoSender,
    }
}
