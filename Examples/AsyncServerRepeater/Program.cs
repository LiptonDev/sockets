﻿
#define AutoAsyncServer

using Sockets;
using Sockets.Enums;
using System;

namespace AsyncServerRepeater
{
    class Program
    {

#if AutoAsyncServer
        static NetServer server = new NetServer(EchoMode.AsyncEchoSender);
#else
        static NetServer server = new NetServer();
#endif

        static void Main(string[] args)
        {
            server.DataValidation = true;

            server.OnClientConnected += server_OnClientConnected;
            server.OnClientDisconnected += server_OnClientDisconnected;

            server.OnReceived += server_OnReceived;

            server.Start(29000);

            Console.WriteLine("Server started");

            while (Console.ReadLine() != "exit") ;
        }

        static void server_OnReceived(NetServer Sender, Guid Client, byte[] Data)
        {
            using (PacketReader reader = new PacketReader(Data))
            {
                Console.WriteLine("Get {0} opcode from {1}, text: {2}", reader.Opcode, Client, reader.ReadUString());

#if !AutoAsyncServer
                Sender.DispatchTo(Client, Data);
#endif
            }
        }

        static void server_OnClientDisconnected(NetServer Sender, Guid Client)
        {
            Console.WriteLine("Client {0} disconnected", Client);
        }

        static void server_OnClientConnected(NetServer Sender, Guid Client)
        {
            Console.WriteLine("Client {0} connected", Client);
        }
    }
}
