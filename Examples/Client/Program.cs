﻿using Sockets;
using System;

namespace Client
{
    class Program
    {
        static NetClient client = new NetClient();
        static void Main(string[] args)
        {
            client.DataValidation = true;

            client.OnReceived += client_OnReceived;
            client.OnConnected += client_OnConnected;
            client.OnDisconnected += client_OnDisconnected;

            client.Connect("localhost", 29000);

            for (int i = 0; i < 100; i++)
            {
                using (PacketWriter writer = new PacketWriter(1337))
                {
                    writer.WriteUString("Message #" + i);

                    client.Send(writer.GetData);
                }
            }

            while (true) ;
        }

        static void client_OnDisconnected(NetClient Sender)
        {
            Console.WriteLine("Disconnected from server");
        }

        static void client_OnConnected(NetClient Sender)
        {
            Console.WriteLine("Connected to server");
        }

        static void client_OnReceived(NetClient Sender, byte[] Data)
        {
            using (PacketReader reader = new PacketReader(Data))
            {
                Console.WriteLine("Get {0} opcode, text: {1}", reader.Opcode, reader.ReadUString());
            }
        }
    }
}
