﻿using System;
using System.Linq;

namespace Sockets
{
    sealed class Convertation
    {
        /// <summary>
        /// Reverse bytes.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] ReverseBytes(byte[] value)
        {
            return FLinq.Reverse(value);
        }

        /// <summary>
        /// string-> octet string (for generated objects, objects, etc., commonly used in games).
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static byte[] ToHexBytes(string hex)
        {
            if (hex == null) return new byte[0];
            if (hex.Length < 2) return new byte[0];

            hex = hex.Replace("-", "");
            hex = hex.Replace(" ", "");
            try
            {
                return Enumerable.Range(0, hex.Length).Where(x => x % 2 == 0).Select(x => Convert.ToByte(hex.Substring(x, 2), 16)).ToArray();
            }
            catch //In the line were extra values.
            {
                return new byte[0];
            }
        }
    }
}
