﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sockets
{
    /// <summary>
    /// Is a class for reading data packet.
    /// </summary>
    public sealed class PacketReader : IDisposable
    {
        /// <summary>
        /// Unique ID of the data packet.
        /// </summary>
        public uint? Opcode { get; private set; }

        bool Reverse = false;

        /// <summary>
        /// Packet data.
        /// </summary>
        Queue<byte> Data { get; set; }

        /// <param name="data">data to read</param>
        /// <param name="readOpcode">read Opcode</param>
        public PacketReader(IEnumerable<byte> data, bool readOpcode = true)
        {
            Data = new Queue<byte>(data);

            if (readOpcode)
            {
                ReadPacketOpcode();
            }
        }

        /// <summary>
        /// Get a value that determines whether there is still data to be read in the packet.
        /// </summary>
        /// <returns></returns>
        public bool HasData()
        {
            return Data.Count > 0;
        }

        /// <summary>
        /// Read Opcode.
        /// </summary>
        void ReadPacketOpcode()
        {
            Opcode = (uint)ReadCUInt32();
        }

        /// <summary>
        /// Read 1 byte If can't read the packet - will return 0.
        /// </summary>
        /// <returns></returns>
        public byte ReadByte()
        {
            if (!HasData())
            {
                return 0;
            }
            else
            {
                return Data.Dequeue();
            }
        }

        /// <summary>
        /// Read the specified number of bytes.
        /// </summary>
        /// <param name="count">the number of bytes to read, value = -1 for automatic definition (ReadCUInt32)</param>
        /// <returns></returns>
        public byte[] ReadBytes(int count)
        {
            byte[] bytes = new byte[count == -1 ? ReadCUInt32() : count];

            for (int i = 0; i < count; i++)
                bytes[i] = ReadByte();

            return !Reverse ? bytes : Convertation.ReverseBytes(bytes);
        }

        /// <summary>
        /// Read Int16 value.
        /// </summary>
        /// <returns></returns>
        public short ReadInt16()
        {
            return BitConverter.ToInt16(ReadBytes(2), 0);
        }

        /// <summary>
        /// Read Int32 value.
        /// </summary>
        /// <returns></returns>
        public int ReadInt32()
        {
            return BitConverter.ToInt32(ReadBytes(4), 0);
        }

        /// <summary>
        /// Read Int64 value.
        /// </summary>
        /// <returns></returns>
        public long ReadInt64()
        {
            return BitConverter.ToInt64(ReadBytes(8), 0);
        }

        /// <summary>
        /// Read UInt16 value.
        /// </summary>
        /// <returns></returns>
        public ushort ReadUInt16()
        {
            return BitConverter.ToUInt16(ReadBytes(2), 0);
        }

        /// <summary>
        /// Read UInt32 value.
        /// </summary>
        /// <returns></returns>
        public uint ReadUInt32()
        {
            return BitConverter.ToUInt32(ReadBytes(4), 0);
        }

        /// <summary>
        /// Read UInt64 value.
        /// </summary>
        /// <returns></returns>
        public ulong ReadUInt64()
        {
            return BitConverter.ToUInt64(ReadBytes(4), 0);
        }

        /// <summary>
        /// Read value with floating point (float).
        /// </summary>
        /// <returns></returns>
        public float ReadSingle()
        {
            return BitConverter.ToSingle(ReadBytes(4), 0);
        }

        /// <summary>
        /// Read string in Unicode encoding.
        /// </summary>
        /// <returns></returns>
        public string ReadUString()
        {
            return Encoding.Unicode.GetString(ReadBytes(ReadCUInt32()));
        }

        /// <summary>
        /// Read octet string.
        /// </summary>
        /// <returns></returns>
        public string ReadOctets()
        {
            return BitConverter.ToString(ReadBytes(ReadCUInt32())).Replace("-", "").ToLower();
        }

        /// <summary>
        /// Read CUInt32 value (usually for the value of the size of the array).
        /// </summary>
        /// <returns></returns>
        public int ReadCUInt32()
        {
            byte b1 = ReadByte();
            if (b1 < 0x80)
                return b1;
            if (b1 < 0xC0)
                return ((b1 << 8) | ReadByte()) & 0x3FFF;
            return ((b1 << 24) | (ReadByte() << 16) | (ReadByte() << 8) | ReadByte()) & 0x1FFFFFFF;
        }

        /// <summary>
        /// Releasing resources.
        /// </summary>
        public void Dispose()
        {
            Opcode = null;

            Data.Clear();
            Data = null;
        }
    }
}
