﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sockets
{
    /// <summary>
    /// Is a class to write a data packet.
    /// </summary>
    public sealed class PacketWriter : IDisposable
    {
        /// <summary>
        /// Packet data.
        /// </summary>
        List<byte> Data = new List<byte>();

        /// <summary>
        /// Unique ID of the data packet.
        /// </summary>
        public uint? Opcode { get; private set; }

        bool Reverse = true;

        /// <param name="Opcode">unique ID of the data packet</param>
        public PacketWriter(uint Opcode)
        {
            this.Opcode = Opcode;
        }

        /// <summary>
        /// Get packet data.
        /// </summary>
        public byte[] GetData
        {
            get
            {
                if (Opcode.HasValue)
                    return CUint32ToBytes(Opcode.Value).Concat(Data).ToArray();
                else return new byte[0];
            }
        }

        /// <summary>
        /// Write 1 byte.
        /// </summary>
        /// <param name="value">value</param>
        public void WriteByte(byte value)
        {
            Data.Add(value);
        }

        /// <summary>
        /// Write byte array.
        /// </summary>
        /// <param name="value">value</param>
        public void WriteBytes(byte[] value)
        {
            Data.AddRange(value);
        }

        /// <summary>
        /// Write Int16 value.
        /// </summary>
        /// <param name="value">value</param>
        public void WriteInt16(short value)
        {
            Data.AddRange(Reverse ? Convertation.ReverseBytes(BitConverter.GetBytes(value)) : BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Write Int32 value.
        /// </summary>
        /// <param name="value">value</param>
        public void WriteInt32(int value)
        {
            Data.AddRange(Reverse ? Convertation.ReverseBytes(BitConverter.GetBytes(value)) : BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Write Int64 value.
        /// </summary>
        /// <param name="value">value</param>
        public void WriteInt64(long value)
        {
            Data.AddRange(Reverse ? Convertation.ReverseBytes(BitConverter.GetBytes(value)) : BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Write UInt16 value.
        /// </summary>
        /// <param name="value">value</param>
        public void WriteUInt16(ushort value)
        {
            Data.AddRange(Reverse ? Convertation.ReverseBytes(BitConverter.GetBytes(value)) : BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Write UInt32 value.
        /// </summary>
        /// <param name="value">value</param>
        public void WriteUInt32(uint value)
        {
            Data.AddRange(Reverse ? Convertation.ReverseBytes(BitConverter.GetBytes(value)) : BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Write UInt64 value.
        /// </summary>
        /// <param name="value">value</param>
        public void WriteUInt64(ulong value)
        {
            Data.AddRange(Reverse ? Convertation.ReverseBytes(BitConverter.GetBytes(value)) : BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Write value with floating point (float).
        /// </summary>
        /// <param name="value">value</param>
        public void WriteSingle(float value)
        {
            Data.AddRange(Reverse ? Convertation.ReverseBytes(BitConverter.GetBytes(value)) : BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Write string in Unicode encoding.
        /// </summary>
        /// <param name="value">value</param>
        public void WriteUString(string value)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(value);
            Data.AddRange(CUint32ToBytes((uint)bytes.Length));
            Data.AddRange(bytes);
        }

        /// <summary>
        /// Write octet string.
        /// </summary>
        /// <param name="value">value</param>
        public void WriteOctet(string value)
        {
            byte[] bytes = Convertation.ToHexBytes(value);
            Data.AddRange(CUint32ToBytes((uint)bytes.Length));
            Data.AddRange(bytes);
        }

        /// <summary>
        /// Write CUInt32 value (usually for the value of the size of the array).
        /// </summary>
        /// <param name="value">value</param>
        public void WriteCUInt32(uint value)
        {
            Data.AddRange(CUint32ToBytes(value));
        }

        /// <summary>
        /// Translate uint value to byte[].
        /// </summary>
        /// <param name="value">value</param>
        /// <returns></returns>
        byte[] CUint32ToBytes(uint value)
        {
            if (value < 0x7F)
            {
                return new[] { (byte)value };
            }
            else if (value < 0x3FFF)
            {
                byte[] array = BitConverter.GetBytes(((short)(value | 0x8000)));
                Array.Reverse(array);
                return array;
            }
            else if (value < 0x1FFFFFFF)
            {
                byte[] array = BitConverter.GetBytes((int)(value | 0xC0000000));
                Array.Reverse(array);
                return array;
            }
            else
            {
                return new byte[0];
            }
        }

        /// <summary>
        /// Releasing resources.
        /// </summary>
        public void Dispose()
        {
            Data.Clear();
            Data = null;

            Opcode = null;
        }
    }
}
