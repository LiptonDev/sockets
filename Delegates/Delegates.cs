﻿using System;

namespace Sockets.Delegates
{
    /// <summary>
    /// Server start.
    /// </summary>
    /// <param name="Sender">server</param>
    public delegate void ServerOnStarted(NetServer Sender);

    /// <summary>
    /// Server stop.
    /// </summary>
    /// <param name="Sender">server</param>
    public delegate void ServerOnStopped(NetServer Sender);

    /// <summary>
    /// New client connection.
    /// </summary>
    /// <param name="Sender">server</param>
    /// <param name="Client">client GUID</param>
    public delegate void ServerOnClientConnected(NetServer Sender, Guid Client);

    /// <summary>
    /// Client disconnect.
    /// </summary>
    /// <param name="Sender">server</param>
    /// <param name="Client">client GUID</param>
    public delegate void ServerOnClientDisconnected(NetServer Sender, Guid Client);

    /// <summary>
    /// Receive data from the client.
    /// </summary>
    /// <param name="Sender">server</param>
    /// <param name="Client">client GUID</param>
    /// <param name="Data">data</param>
    public delegate void ServerOnReceived(NetServer Sender, Guid Client, byte[] Data);

    /// <summary>
    /// Start client stream.
    /// </summary>
    /// <param name="Sender">server</param>
    /// <param name="Client">client GUID</param>
    delegate void StreamOnStarted(NetStream Sender, Guid Client);

    /// <summary>
    /// Stop client stream.
    /// </summary>
    /// <param name="Sender">server</param>
    /// <param name="Client">client GUID</param>
    delegate void StreamOnStopped(NetStream Sender, Guid Client);

    /// <summary>
    /// Receive data from the client.
    /// </summary>
    /// <param name="Sender">server</param>
    /// <param name="Client">client GUID</param>
    /// <param name="Data">данные</param>
    delegate void StreamOnReceived(NetStream Sender, Guid Client, byte[] Data);

    /// <summary>
    /// Connected to server.
    /// </summary>
    /// <param name="Sender">client</param>
    public delegate void ClientOnConnected(NetClient Sender);

    /// <summary>
    /// Disconnected to server.
    /// </summary>
    /// <param name="Sender">client</param>
    public delegate void ClientOnDisconnected(NetClient Sender);

    /// <summary>
    /// Receive data from the server.
    /// </summary>
    /// <param name="Sender">client</param>
    /// <param name="Data">data</param>
    public delegate void ClientOnReceived(NetClient Sender, byte[] Data);
}
