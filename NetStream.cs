﻿using Sockets.Delegates;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Sockets
{
    /// <summary>
    /// Network stream.
    /// </summary>
    public class NetStream
    {
        internal const int KByte = 1024;
        internal const int MByte = KByte * KByte;
        internal const int BufferSize = 8 * KByte;
        internal const int MaxDataLength = 10 * MByte;

        /// <summary>
        /// Gets or sets use validation data.
        /// </summary>
        internal static bool UseDataValidation { get; set; }

        /// <summary>
        /// Validation data.
        /// </summary>
        static byte[] DataValidation = { 
                                          0xE6, 0xF4, 0x49, 0x3D, 0xD8,
                                          0x5A, 0x8, 0xB1, 0x6C, 0x9D,
                                          0x26, 0x3C, 0x29, 0xAC, 0x4C,
                                          0xD3, 0x28, 0xF8, 0xE1, 0x71,
                                          0x7B, 0x9, 0x32, 0x86, 0x6D,
                                          0xBE, 0x71, 0x9B, 0x89, 0xCC,
                                          0x12, 0x4B
                                      };

        /// <summary>
        /// Client connection time.
        /// </summary>
        public DateTime TimeConnected { get; private set; }

        /// <summary>
        /// Network stream.
        /// </summary>
        NetworkStream stream;

        /// <summary>
        /// Client GUID.
        /// </summary>
        public Guid Guid { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the stream is active.
        /// </summary>
        public bool IsActive { get; private set; }

        /// <summary>
        /// Gets the remote endpoint.
        /// </summary>
        public EndPoint EndPoint { get; private set; }

        /// <summary>
        /// Tick rate.
        /// </summary>
        int? tickRate = 1;

        /// <summary>
        /// Occurs when the client connected.
        /// </summary>
        internal event StreamOnStarted OnStarted = delegate { };

        /// <summary>
        /// Occurs when the client disconnected.
        /// </summary>
        internal event StreamOnStopped OnStopped = delegate { };

        /// <summary>
        /// Occurs when data is received from the client.
        /// </summary>
        internal event StreamOnReceived OnReceived = delegate { };

        /// <summary>
        /// Initializes a new instance of the NetStream class.
        /// </summary>
        /// <param name="stream">network stream</param>
        /// <param name="endPoint">remote endpoint</param>
        internal NetStream(NetworkStream stream, EndPoint endPoint)
        {
            Guid = Guid.NewGuid();
            IsActive = false;
            EndPoint = endPoint;
            this.stream = stream;

            TimeConnected = DateTime.Now;
        }

        /// <summary>
        /// Starts the stream.
        /// </summary>
        internal void Start()
        {
            IsActive = true;

            OnStarted(this, Guid);

            Receive();
        }

        /// <summary>
        /// Stops the stream.
        /// </summary>
        internal void Stop()
        {
            if (!IsActive) return;

            IsActive = false;

            stream.Close();
            stream.Dispose();
            stream = null;

            EndPoint = null;
            tickRate = null;

            OnStopped(this, Guid);
        }

        /// <summary>
        /// Send data.
        /// </summary>
        /// <param name="data">data</param>
        internal void Send(byte[] data)
        {
            if (!IsActive) return;
            if (!stream.CanWrite) return;

            try
            {
                data = GetPayload(data);
                stream.Write(data, 0, data.Length);
            }
            catch
            {
                Stop();
            }
        }

        /// <summary>
        /// Write validation data.
        /// </summary>
        /// <param name="data">data</param>
        /// <returns></returns>
        byte[] GetPayload(byte[] data)
        {
            byte[] length = BitConverter.GetBytes(data.Length + (UseDataValidation ? DataValidation.Length : 0));
            int len = length.Length + (UseDataValidation ? DataValidation.Length : 0);
            byte[] newBuffer = new byte[data.Length + len];

            Buffer.BlockCopy(length, 0, newBuffer, 0, length.Length);

            if (UseDataValidation)
                Buffer.BlockCopy(DataValidation, 0, newBuffer, length.Length, DataValidation.Length);

            Buffer.BlockCopy(data, 0, newBuffer, len, data.Length);

            return newBuffer;
        }

        /// <summary>
        /// Read data from the stream.
        /// </summary>
        async void Receive()
        {
            while (IsActive && stream.CanRead)
            {
                await Task.Delay(tickRate.Value);

                byte[] buffer = new byte[BufferSize];
                int readCount = 0;

                try
                {
                    readCount = await stream.ReadAsync(buffer, 0, BufferSize);
                    if (readCount == 0)
                    {
                        Stop();
                        return;
                    }
                }
                catch (Exception)
                {
                    Stop();
                    return;
                }

                if (readCount < BufferSize)
                {
                    byte[] num2 = new byte[readCount];
                    Buffer.BlockCopy(buffer, 0, num2, 0, readCount);
                    buffer = num2;
                }

                BufferHandler(buffer);
            }

            Stop();
        }

        /// <summary>
        /// Temporary buffer.
        /// </summary>
        byte[] buffer = new byte[0];

        /// <summary>
        /// Waiting for all data.
        /// </summary>
        /// <param name="bytes">data</param>
        void BufferHandler(byte[] bytes)
        {
            int newLen = buffer.Length + bytes.Length;
            byte[] newBuffer = new byte[newLen];
            Buffer.BlockCopy(buffer, 0, newBuffer, 0, buffer.Length);
            Buffer.BlockCopy(bytes, 0, newBuffer, buffer.Length, bytes.Length);
            buffer = newBuffer;

            while (buffer.Length >= 4)
            {
                byte[] lenBytes = new byte[4];
                Buffer.BlockCopy(buffer, 0, lenBytes, 0, 4);
                int len = BitConverter.ToInt32(lenBytes, 0);

                if (buffer.Length >= 4 + len && len > 0 && len < MaxDataLength)
                {
                    byte[] payload = new byte[len];
                    Buffer.BlockCopy(buffer, 4, payload, 0, len);

                    newLen = buffer.Length - 4 - len;
                    newBuffer = new byte[newLen];
                    int offset = 4 + len;
                    Buffer.BlockCopy(buffer, offset, newBuffer, 0, newLen);
                    buffer = newBuffer;

                    if (!UseDataValidation)
                    {
                        OnReceived(this, Guid, payload);
                    }
                    else
                    {
                        try
                        {
                            byte[] pData = new byte[DataValidation.Length];
                            Buffer.BlockCopy(payload, 0, pData, 0, pData.Length);

                            byte[] newData = new byte[payload.Length - pData.Length];
                            Buffer.BlockCopy(payload, pData.Length, newData, 0, newData.Length);

                            if (pData.SequenceEqual(DataValidation))
                            {
                                OnReceived(this, Guid, newData);
                            }
                            else
                            {
                                Stop();
                            }
                        }
                        catch
                        {
                            Stop();
                        }
                    }
                }
                else
                {
                    break;
                }
            }
        }
    }
}
